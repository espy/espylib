#!/usr/bin/env bash

# -------------------- pre -------------------- #

NOM="espylib"

ORIGINAL_WD="${PWD}"
SCRIPT_PARENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# echo "SCRIPT_PARENT_DIR=${SCRIPT_PARENT_DIR}"
cd "${SCRIPT_PARENT_DIR}"
cd ..

# -------------------- work -------------------- #


# python3 -m pip install --user --upgrade setuptools wheel
# python3 setup.py sdist bdist_wheel
pwd

./script/test.sh
rm -rf espylib/__pycache__
python ./setup.py sdist bdist_wheel



# prep host for upload...
# python3 -m pip install --user --upgrade twine

# upload
# python3 -m twine upload --repository testpypi dist/*

#use here:
# python3 -m pip install --index-url https://test.pypi.org/simple/ --no-deps libespy

# python3 setup.py sdist bdist_wheel
# python3 -m twine upload --repository testpypi dist/*
