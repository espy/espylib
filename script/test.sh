#!/usr/bin/env bash

# -------------------- pre -------------------- #

NOM="espylib"

ORIGINAL_WD="${PWD}"
SCRIPT_PARENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# echo "SCRIPT_PARENT_DIR=${SCRIPT_PARENT_DIR}"
cd "${SCRIPT_PARENT_DIR}"
cd ..

# -------------------- work -------------------- #


pwd
rm -rf espylib/__pycache__
nosetests
