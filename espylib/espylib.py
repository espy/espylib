
# -------------------- imports -------------------- #

trace = True
debug = True
info = True

# -------------------- b -------------------- #

def removeStringFromString(base_str, str_to_remove):
    result_str = base_str.replace(str_to_remove, '') 
    return result_str
    
def removeLeadingNumbersFromString(in_string):
    if not in_string: return ''
    in_string = in_string.strip()
    if len(in_string) < 1: return ''
    while in_string[0].isnumeric():
        in_string = in_string[1:].strip()
    out_string = in_string
    return out_string
    
# Change base_str into new string with all str_to_remove replaced
# with replace_with_str. Alwasys returns a string.
def changeStringsInString(base_str, str_to_remove, replace_with_str):
    if not base_str: return ''
    if not str_to_remove: return ''
    if not type(base_str) == type(''): return ''
    # if not replace_with_str: return None
    result_str = base_str.replace(str_to_remove, replace_with_str) 
    return result_str

def assureNonNullString(in_string):

    # this handles None and type mismatch too...
    if not isinstance(in_string, (str)):
        return ''
    
    if len(in_string) < 1:
        return ''
    return in_string


def makeBatch(in_list, maxSizePer):
    lists_list = []
    

    # address corner cases...
    if in_list is None: 
        if debug: print('No list passed to batchify')
        return None
    if maxSizePer is None: 
        if debug: print('No batch size passed')
        return None


    if len(in_list) < maxSizePer: 
        lists_list.append(in_list)
        return lists_list
    if not maxSizePer: return None
    if maxSizePer < 1 : return None


    current_counter = 0
    short_list = []
    for item in in_list:
        
        if len(short_list) >= maxSizePer:
            lists_list.append(short_list)
            short_list = []
        short_list.append(item)
        current_counter += 1
    
    if len(short_list) > 0:
        lists_list.append(short_list)

    return lists_list

