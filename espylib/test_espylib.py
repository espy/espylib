from espylib import espylib

# -------------------- a -------------------- #

def test_changeStringsInString():
    assert espylib.changeStringsInString('bobAbob', 'A','') == 'bobbob'
    assert espylib.changeStringsInString('', 'A','') == ''
    assert espylib.changeStringsInString(None, 'A','') == ''
    assert espylib.changeStringsInString(None, None, None) == ''
    assert espylib.changeStringsInString(None, 'A', None) == ''
    assert espylib.changeStringsInString('', '','') == ''
    assert espylib.changeStringsInString('A', 'A','') == ''
    assert espylib.changeStringsInString('A', 'A','1') == '1'
    assert espylib.changeStringsInString('AA', 'A', '1') == '11'
    assert espylib.changeStringsInString('AA', 'A', '') == ''
    assert espylib.changeStringsInString(['aaa', 'bbbb'], 'A', '') == ''

def test_assureNonNullString():
    assert espylib.assureNonNullString(None) == ''
    assert espylib.assureNonNullString('') == ''
    assert espylib.assureNonNullString('misc') == 'misc'
    # assert espylib.assureNonNullString(None) == ''

def test_makeBatch():
    maxSizePer = 10

    assert espylib.makeBatch(None, maxSizePer) == None

    test_list_1 = [i for  i in range(1, 101)]
    assert espylib.makeBatch(test_list_1, None) == None
    assert espylib.makeBatch(None, None) == None

    assert len(test_list_1) == 100
    assert test_list_1[0] == 1
    assert test_list_1[10] == 11
    assert test_list_1[99] == 100
    # there is no test_list[100] BTW


    test_list_2 = [i for  i in range(1, 101)]
    assert len(test_list_1) == 100

    a = espylib.makeBatch(test_list_2, maxSizePer)
    assert len(a) == 10
    assert len(a[0]) == 10
    assert len(a[9]) == 10
    assert a[9] == [91, 92, 93, 94, 95, 96, 97, 98, 99, 100]
    assert a[0][0] == 1
    assert a[0][1] == 2
    assert a[1][0] == 11
    assert a[1][9] == 20
    assert a[9][0] == 91
    assert a[9][9] == 100
    # there is no a[9][10] BTW
    # there is no a[10] BTW

    # assert a[0][1] == 2

    test_list_2 = [i for  i in range(1, 122)]
    assert len(test_list_1) == 100

    b = espylib.makeBatch(test_list_2, maxSizePer)
    assert len(b) == 13
    assert b[12][0] == 121
    assert b[12][0] == 121




    # assert a[10][10] == 100
    # assert test_list[1] == 4
    # assert len(a) == 11

    # assert espylib.makeBatch(None, maxSizePer) == None
    # assert len(espylib.makeBatch([], maxSizePer)) == 1
    # assert len(espylib.makeBatch(test_list, maxSizePer)) == 10
